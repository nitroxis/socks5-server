﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace SocksServer
{
	public enum Command
	{
		Connect = 1,
		Bind = 2,
		UdpAssociate = 3,
	}

	public enum AddressType
	{
		IPv4 = 1,
		Hostname = 3,
		IPv6 = 4,
	}

	public enum Reply
	{
		Success,
		SocksServerFailure,
		ConnectionNotAllowed,
		NetworkUnreachable,
		HostUnreachable,
		ConnectionRefused,
		TTLExpired,
		CommandNotSupported,
		AddressTypeNotSupported
	}

	public class Config
	{
		public string ListenAddress { get; set; }
		public string SourceAddress { get; set; }

		public string User { get; set; }
		public string Password { get; set; }
	}

	public static class Program
	{
		public static async Task Main(string[] args)
		{
			CancellationTokenSource cts = new CancellationTokenSource();

			void onCancelKeyPress(object sender, ConsoleCancelEventArgs e)
			{
				e.Cancel = true;
				cts.Cancel();
				Console.CancelKeyPress -= onCancelKeyPress;
			}

			Console.CancelKeyPress += onCancelKeyPress;

			IConfigurationRoot config = new ConfigurationBuilder()
				.AddEnvironmentVariables(o =>
				{
					o.Prefix = "Socks_";
				})
				.AddCommandLine(args)
				.Build();

			await run(config.Get<Config>(), cts.Token);
		}

		private static async Task runServer(TcpListener listener, Config config, CancellationToken cancellationToken)
		{
			Console.WriteLine($"Listening on {listener.LocalEndpoint}");

			listener.Start();
			cancellationToken.Register(listener.Stop);

			List<Task> tasks = new List<Task>();

			try
			{
				for (;;)
				{
					cancellationToken.ThrowIfCancellationRequested();

					TcpClient client;
					try
					{
						client = await listener.AcceptTcpClientAsync();
					}
					catch (ObjectDisposedException)
					{
						break;
					}
					
					EndPoint ep = client.Client.RemoteEndPoint;
					
					Console.WriteLine($"+ {ep}");

					lock (tasks)
					{
						
						Task clientTask = handleClient(client, config, cancellationToken);
						tasks.Add(clientTask);
						clientTask.ContinueWith(t =>
						{
							lock (tasks)
							{
								tasks.Remove(clientTask);
							}

							Console.WriteLine($"- {ep}");

							if (t.Exception != null)
							{
								Console.WriteLine(t.Exception.ToString());
							}
						});
					}
				}
			}
			finally
			{
				await Task.WhenAll(tasks);
			}
		}

		private static async Task run(Config config, CancellationToken cancellationToken)
		{
			string[] addresses = config.ListenAddress.Split(';');
			if (addresses.Length == 0)
				throw new ArgumentException("At least one listen address is required.", nameof(config));

			Task[] tasks = new Task[addresses.Length];

			for (int i = 0; i < addresses.Length; ++i)
			{
				TcpListener s = new TcpListener(IPEndPoint.Parse(addresses[i]));
				tasks[i] = runServer(s, config, cancellationToken);
			}

			await Task.WhenAll(tasks);
		}

		private static async ValueTask ReadAll(this Stream stream, Memory<byte> memory, CancellationToken cancellationToken)
		{
			while (memory.Length > 0)
			{
				int n = await stream.ReadAsync(memory, cancellationToken);
				if (n == 0)
					throw new EndOfStreamException();

				memory = memory.Slice(n);
			}
		}

		private static int getShort(ReadOnlySpan<byte> buf)
		{
			return (buf[0] << 8) | buf[1];
		}

		private static int buildReply(Span<byte> dest, Reply reply, AddressType atyp, ReadOnlySpan<byte> addr, int port)
		{
			dest[0] = 5;
			dest[1] = (byte)reply;
			dest[2] = 0;
			dest[3] = (byte)atyp;
			addr.CopyTo(dest.Slice(4, addr.Length));
			dest[4 + addr.Length] = (byte)(port >> 8);
			dest[5 + addr.Length] = (byte)(port);

			return addr.Length + 6;
		}

		private static async Task writeReply(Stream stream, Memory<byte> temp, Reply reply, ReadOnlyMemory<byte> addr, int port, CancellationToken cancellationToken)
		{
			AddressType atyp = addr.Length == 4 ? AddressType.IPv4 : AddressType.IPv6;
			int replyLength = buildReply(temp.Span, reply, atyp, addr.Span, port);
			await stream.WriteAsync(temp.Slice(0, replyLength), cancellationToken);
		}

		private static async Task handleClient(TcpClient client, Config config, CancellationToken cancellationToken)
		{
			await using Stream stream = client.GetStream();

			// Setup some temporary memory.
			Memory<byte> temp = new byte[4096];
			Memory<byte> replyTemp = temp.Slice(2048);
			Memory<byte> addrTemp = temp.Slice(4096 - 16);
			temp = temp.Slice(0, 2048);

			// Read version and methods.
			await stream.ReadAll(temp.Slice(0, 2), cancellationToken);
			int version = temp.Span[0];
			if (version != 5)
				throw new InvalidDataException($"Invalid socks version, expected version 5, got {version}.");

			// Read methods.
			int numMethods = temp.Span[1];
			await stream.ReadAll(temp.Slice(0, numMethods), cancellationToken);

			// Determine if username/password auth is required.
			byte requiredMethod = 0;
			if (!string.IsNullOrEmpty(config.User))
			{
				requiredMethod = 2;
			}

			if (!temp.Slice(0, numMethods).Span.Contains(requiredMethod))
			{
				// No acceptable methods.
				replyTemp.Span[0] = 0x05;
				replyTemp.Span[1] = 0xFF;
				await stream.WriteAsync(replyTemp.Slice(0, 2), cancellationToken);

				// Exit.
				return;
			}
			
			// Answer with method.
			replyTemp.Span[0] = 0x05;
			replyTemp.Span[1] = requiredMethod;
			await stream.WriteAsync(replyTemp.Slice(0, 2), cancellationToken);
		
			switch (requiredMethod)
			{
				case 0:
					break;

				case 2:
					// 0x0101 means unsuccessful login.
					replyTemp.Span[0] = 0x01;
					replyTemp.Span[1] = 0x01;

					await stream.ReadAll(temp.Slice(0, 2), cancellationToken);
					version = temp.Span[0];
					if (version != 1)
					{
						await stream.WriteAsync(replyTemp.Slice(0, 2), cancellationToken);
						return;
					}

					int ulen = temp.Span[1];
					await stream.ReadAll(temp.Slice(0, ulen + 1), cancellationToken);

					string uname = Encoding.UTF8.GetString(temp.Slice(0, ulen).Span);

					int plen = temp.Span[ulen];
					await stream.ReadAll(temp.Slice(0, plen), cancellationToken);

					string passwd = Encoding.UTF8.GetString(temp.Slice(0, plen).Span);

					if (!string.Equals(uname, config.User, StringComparison.Ordinal) || !string.Equals(passwd, config.Password, StringComparison.Ordinal))
					{
						await stream.WriteAsync(replyTemp.Slice(0, 2), cancellationToken);
						return;
					}

					// 0x0100 means success.
					replyTemp.Span[1] = 0x00;
					await stream.WriteAsync(replyTemp.Slice(0, 2), cancellationToken);

					break;

				default:
					throw new NotSupportedException();
			}

			// Read request.
			await stream.ReadAll(temp.Slice(0, 4), cancellationToken);
			version = temp.Span[0];
			if (version != 5)
				throw new InvalidDataException($"Invalid request version, expected version 5, got {version}.");

			Command cmd = (Command)temp.Span[1];
			AddressType atyp = (AddressType)temp.Span[3];

			IPEndPoint remoteEp = null;
			IPEndPoint localEp = null;

			if (config.SourceAddress != null)
			{
				localEp = IPEndPoint.Parse(config.SourceAddress);
			}

			switch (atyp)
			{
				case AddressType.IPv4:
					await stream.ReadAll(temp.Slice(4, 4 + 2), cancellationToken);
					remoteEp = new IPEndPoint(new IPAddress(temp.Span.Slice(4, 4)), getShort(temp.Span.Slice(8, 2)));
					break;

				case AddressType.Hostname:
					await stream.ReadAll(temp.Slice(4, 1), cancellationToken);
					int len = temp.Span[4];
					await stream.ReadAll(temp.Slice(5, len + 2), cancellationToken);
					string dns = Encoding.UTF8.GetString(temp.Span.Slice(5, len));
					int port = getShort(temp.Span.Slice(5 + len, 2));
					try
					{
						// Resolve host.
						IPAddress[] hostAddrs = await Dns.GetHostAddressesAsync(dns);

						// Find best match.
						IPAddress match;
						if (localEp != null)
							match = hostAddrs.FirstOrDefault(a => a.AddressFamily == localEp.AddressFamily) ?? hostAddrs.FirstOrDefault();
						else
							match = hostAddrs.FirstOrDefault();

						// Get remote EP.
						if (match != null)
							remoteEp = new IPEndPoint(match, port);
					}
					catch (SocketException)
					{
						// "An error is encountered when resolving hostNameOrAddress"
					}
					catch (ArgumentException)
					{
						// "hostNameOrAddress is an invalid IP address"
						await writeReply(stream, replyTemp, Reply.NetworkUnreachable, addrTemp.Slice(0, 4), 0, cancellationToken);
						return;
					}

					break;

				case AddressType.IPv6:
					await stream.ReadAll(temp.Slice(4, 16 + 2), cancellationToken);
					remoteEp = new IPEndPoint(new IPAddress(temp.Span.Slice(4, 16)), getShort(temp.Span.Slice(20, 2)));
					break;

				default:
					await writeReply(stream, replyTemp, Reply.AddressTypeNotSupported, addrTemp.Slice(0, 4), 0, cancellationToken);
					return;
			}

			if (remoteEp == null)
			{
				await writeReply(stream, replyTemp, Reply.HostUnreachable, addrTemp.Slice(0, 4), 0, cancellationToken);
				return;
			}

			if (localEp == null)
			{
				if (remoteEp.AddressFamily == AddressFamily.InterNetworkV6)
					localEp = new IPEndPoint(IPAddress.IPv6Any, 0);
				else
					localEp = new IPEndPoint(IPAddress.Any, 0);
			}

			Memory<byte> addr = addrTemp;
			if (localEp.Address.TryWriteBytes(addrTemp.Span, out int alen))
				addr = addrTemp.Slice(0, alen);

			switch (cmd)
			{
				case Command.Connect:
					TcpClient proxyClient = new TcpClient(localEp);

					try
					{
						await proxyClient.ConnectAsync(remoteEp.Address, remoteEp.Port);
					}
					catch (SocketException e)
					{
						switch (e.SocketErrorCode)
						{
							case SocketError.AccessDenied:
								await writeReply(stream, replyTemp, Reply.ConnectionNotAllowed, addr, 0, cancellationToken);
								return;

							case SocketError.ProtocolNotSupported:
							case SocketError.ProtocolFamilyNotSupported:
							case SocketError.AddressFamilyNotSupported:
							case SocketError.SocketNotSupported:
							case SocketError.VersionNotSupported:
								await writeReply(stream, replyTemp, Reply.AddressTypeNotSupported, addr, 0, cancellationToken);
								return;

							case SocketError.NetworkReset:
							case SocketError.ConnectionRefused:
							case SocketError.ConnectionReset:
								await writeReply(stream, replyTemp, Reply.ConnectionRefused, addr, 0, cancellationToken);
								return;

							case SocketError.HostNotFound:
							case SocketError.HostUnreachable:
							case SocketError.TimedOut:
								await writeReply(stream, replyTemp, Reply.HostUnreachable, addr, 0, cancellationToken);
								return;

							case SocketError.NetworkUnreachable:
								await writeReply(stream, replyTemp, Reply.NetworkUnreachable, addr, 0, cancellationToken);
								return;

							case SocketError.OperationNotSupported:
								await writeReply(stream, replyTemp, Reply.CommandNotSupported, addr, 0, cancellationToken);
								return;

							default:
								await writeReply(stream, replyTemp, Reply.SocksServerFailure, addr, 0, cancellationToken);
								return;
						}
					}
					catch (Exception)
					{
						await writeReply(stream, replyTemp, Reply.SocksServerFailure, addr, 0, cancellationToken);
						return;
					}

					localEp = (IPEndPoint)proxyClient.Client.LocalEndPoint;

					if (localEp.Address.TryWriteBytes(addrTemp.Span, out alen))
						addr = addrTemp.Slice(0, alen);

					await writeReply(stream, replyTemp, Reply.Success, addr, 0, cancellationToken);

					await using (Stream proxyStream = proxyClient.GetStream())
					{
						async Task s2c()
						{
							await Task.Yield();
							await stream.CopyToAsync(proxyStream, cancellationToken);
							client.Client.Shutdown(SocketShutdown.Receive);
							proxyClient.Client.Shutdown(SocketShutdown.Send);
						}

						async Task c2s()
						{
							await Task.Yield();
							await proxyStream.CopyToAsync(stream, cancellationToken);
							proxyClient.Client.Shutdown(SocketShutdown.Receive);
							client.Client.Shutdown(SocketShutdown.Send);
						}

						await Task.WhenAll(s2c(), c2s());
					}

					break;

				default:
					await writeReply(stream, replyTemp, Reply.CommandNotSupported, addr, 0, cancellationToken);
					return;
			}
		}
	}
}
