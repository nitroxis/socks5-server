FROM mcr.microsoft.com/dotnet/core/runtime:3.0-alpine3.9 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.0-alpine3.9 AS build
WORKDIR /src
COPY ["SocksServer/SocksServer.csproj", "SocksServer/"]
RUN dotnet restore "SocksServer/SocksServer.csproj"
COPY . .
WORKDIR "/src/SocksServer"
RUN dotnet build "SocksServer.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "SocksServer.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "SocksServer.dll"]